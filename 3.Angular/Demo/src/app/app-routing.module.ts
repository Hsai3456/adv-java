import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ShowEmployeesComponent } from './show-employees/show-employees.component';
import { ShowEmpByIdComponent } from './show-emp-by-id/show-emp-by-id.component';
import { ProductsComponent } from './products/products.component';
import { LogoutComponent } from './logout/logout.component';
import { authGuard } from './auth.guard';
import { HammerModule } from '@angular/platform-browser';
import { HomePageComponent } from './home-page/home-page.component';
import { CartComponent } from './cart/cart.component';


const routes: Routes = [
  {path:'',            component:LoginComponent},
  {path:'login',       component:LoginComponent},
  {path:'register',    component:RegisterComponent},
  {path:'cart',    component:CartComponent},
  {path:'showemps',    canActivate:[authGuard], component:ShowEmployeesComponent},
  {path:'showempbyid', canActivate:[authGuard], component:ShowEmpByIdComponent},
  {path:'products',    canActivate:[authGuard], component:ProductsComponent},
  {path:'logout',      canActivate:[authGuard], component:LogoutComponent},
  //{path: 'homePage',  component:HomePageComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
