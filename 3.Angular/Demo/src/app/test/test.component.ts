import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {

  id: number;
  name: string;
  avg: number;
  address : any;
  hobbies : any;

  constructor(){
    //alert("constructor invoked...")
    this.id = 101;
    this.name = 'Hemanth';
    this.avg = 78.88;

    this.address = {
      streetNo: 101,
      city: 'Hyderabad',
      state:'Telangana'
    };

    this.hobbies= ['music','movies','Reading','Eatings']
  }


  ngOnInit() {
    //alert("ngOnInit invoked...")
  }
}
