import { Component } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent {
  isUserLoggedIn: boolean | undefined;
  cartItems: any;

  constructor(private service: EmpService) {
    this.cartItems = service.getCartItems();
  }

  ngOnInit() {
    this.service.getIsUserLogged().subscribe((data: boolean) => {
      this.isUserLoggedIn= data;
    });
  }
}
