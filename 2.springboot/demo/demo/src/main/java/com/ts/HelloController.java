package com.ts;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@RequestMapping("Hello1")
	public String Hello1(){
		return "Hi EVeryOne";
	}
	
	@RequestMapping("Hello")
	public String Hello(){
		return "Hi EveryOne";
	}
}
